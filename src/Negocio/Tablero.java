package Negocio;

import java.util.ArrayList;
import java.util.Random;

public class Tablero {
	ArrayList<Celda> tablero;
	int tama�o;
	int cantColumnas;

	public Tablero(int cantColumnas) {
		tablero = new ArrayList<Celda>();
		this.tama�o = cantColumnas * cantColumnas;
		this.cantColumnas = cantColumnas;

		for (int i = 0; i < this.tama�o; i++) {
			Celda celda = new Celda();
			tablero.add(celda);
		}
	}

	int tama�o() {
		return this.tama�o;
	}

	public String toString() {

		String mostrarTablero = "";
		int cont = 0;
		for (int i = 0; i < tama�o(); i++) {
			mostrarTablero += this.tablero.get(i).getValor() + " ";
			cont++;
			if (cont == cantColumnas) {
				cont = 0;
				mostrarTablero += "\n";
			}
		}
		return mostrarTablero;
	}

	void nuevoValor() {
		Random rdm = new Random();
		int pos = rdm.nextInt(tama�o);
		while (tablero.get(pos).ocupado()) {
			pos = rdm.nextInt(tama�o);
			if (!tablero.get(pos).ocupado()) {
				break;
			}
		}
		tablero.get(pos).sumar(2); // AGREGAR MAS VALORES
	}

	boolean lleno() {
		for (Celda celda : tablero) {
			if (!celda.ocupado()) {
				return false;
			}
		}
		return true;

	}

	boolean ocupado(int i) {
		return tablero.get(i).ocupado();
	}

	int valorDeCelda(int i) {
		return tablero.get(i).getValor();
	}

	boolean mismoValor(int desde, int hacia) {
		return (this.valorDeCelda(hacia) == this.valorDeCelda(desde));
	}

	// MOVIMIENTOS

	void moverArr() {
		for (int desde = cantColumnas; desde < tama�o(); desde++) {
			if (ocupado(desde)) {
				int hacia = desde % cantColumnas;
				if (ocupado(hacia)) {
					if (this.mismoValor(desde, hacia)) {
						this.mover(desde, hacia);
					} else {
						if ((hacia + cantColumnas) < desde) {
							if (ocupado(hacia + cantColumnas)) {
								if (this.mismoValor(desde, hacia + cantColumnas)) {
									this.mover(desde, hacia + cantColumnas);
								} else {
									if ((hacia + cantColumnas * 2) < desde) {
										if (ocupado(hacia + cantColumnas * 2)) {
											if (this.mismoValor(desde, hacia + cantColumnas * 2)) {
												this.mover(desde, hacia + cantColumnas * 2);
											}
										} else {
											this.mover(desde, hacia + cantColumnas * 2);
										}
									}
								}
							} else {
								this.mover(desde, hacia + cantColumnas);
							}
						}
					}
				} else {
					this.mover(desde, hacia);
				}
			}
		}
	}

	void moverAba() {
		int celdaAbajoIzquierda = tama�o - cantColumnas;
		for (int desde = celdaAbajoIzquierda - 1; desde >= 0; desde--) {
			if (ocupado(desde)) {
				int hacia = (desde % cantColumnas) + celdaAbajoIzquierda;
				if (ocupado(hacia)) {
					if (this.mismoValor(desde, hacia)) {
						this.mover(desde, hacia);
					} else {
						if ((hacia - cantColumnas) > desde) {
							if (ocupado(hacia - cantColumnas)) {
								if (this.mismoValor(desde, hacia - cantColumnas)) {
									this.mover(desde, hacia - cantColumnas);
								} else {
									if ((hacia - cantColumnas * 2) > desde) {
										if (ocupado(hacia - cantColumnas * 2)) {
											if (this.mismoValor(desde, hacia - cantColumnas * 2)) {
												this.mover(desde, hacia - cantColumnas * 2);
											}
										} else {
											this.mover(desde, hacia - cantColumnas * 2);
										}
									}
								}
							} else {
								this.mover(desde, hacia - cantColumnas);
							}
						}
					}
				} else {
					this.mover(desde, hacia);
				}
			}
		}
	}

	void moverDer() {
		for (int desde = tama�o - 2; desde >= 0; desde--) {
			if ((desde + 1) % cantColumnas != 0) {
				if (ocupado(desde)) {
					int hacia = metAuxHaciaDer(desde);
					if (ocupado(hacia)) {
						if (this.mismoValor(desde, hacia)) {
							this.mover(desde, hacia);
						} else {
							buscarCeldaLibreDer(desde, hacia);
						}
					} else {
						this.mover(desde, hacia);
					}
				}
			}
		}
	}

	int metAuxHaciaDer(int desde) {

		for (int i = 0; i < cantColumnas; i++) {
			if (desde < cantColumnas * (i + 1)) {
				return (cantColumnas * (i + 1)) - 1;
			}
		}

		return desde;

	}

	void buscarCeldaLibreDer(int desde, int hacia) {
		if ((hacia - 1) > desde) {
			if (ocupado(hacia - 1)) {
				if (this.mismoValor(desde, hacia - 1)) {
					this.mover(desde, hacia - 1);
				} else {
					buscarCeldaLibreDer(desde, hacia - 1);
				}
			} else {
				this.mover(desde, hacia - 1);
			}
		}

	}

	void moverIzq() {
		for (int desde = 1; desde < tama�o(); desde++) {
			if (desde % cantColumnas != 0) {
				if (ocupado(desde)) {
					int hacia = metAuxHaciaIzq(desde);
					if (ocupado(hacia)) {
						if (this.mismoValor(desde, hacia)) {
							this.mover(desde, hacia);
						} else {
							buscarCeldaLibreIzq(desde, hacia);
						}
					} else {
						this.mover(desde, hacia);
					}
				}
			}
		}
	}

	int metAuxHaciaIzq(int desde) {

		for (int i = cantColumnas; i > 0; i--) {
			if (desde > cantColumnas * (i - 1)) {
				return cantColumnas * (i - 1);
			}
		}

		return desde;

	}

	void buscarCeldaLibreIzq(int desde, int hacia) {
		if ((hacia + 1) < desde) {
			if (ocupado(hacia + 1)) {
				if (this.mismoValor(desde, hacia + 1)) {
					this.mover(desde, hacia + 1);
				} else {
					buscarCeldaLibreDer(desde, hacia + 1);
				}
			} else {
				this.mover(desde, hacia + 1);
			}
		}

	}

	void mover(int desde, int hacia) {
		tablero.get(hacia).sumar(tablero.get(desde).getValor());
		tablero.get(desde).anular();
	}

	public static void main(String[] args) {
		Tablero tablero = new Tablero(5);
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();

		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();
		tablero.nuevoValor();

		System.out.println(tablero.toString());
		// System.out.println(tablero.tama�o());
		// System.out.println(tablero.lleno());
		tablero.moverIzq();
		System.out.println(tablero.toString());
		tablero.moverDer();
		System.out.println(tablero.toString());
		tablero.moverArr();
		System.out.println(tablero.toString());
		tablero.moverAba();
		System.out.println(tablero.toString());

	}

}
