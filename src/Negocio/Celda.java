package Negocio;

public class Celda {

	int valor;

	public Celda() {
		this.valor = 0;
	}

	int getValor() {
		return this.valor;
	}

	void sumar(int valor) {
		this.valor += valor;
	}

	void anular() {
		this.valor = 0;
	}

	boolean ocupado() {
		return this.valor != 0;
	}
}
